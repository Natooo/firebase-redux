import React, { Component } from 'react'
import { database } from "../firebase"
import _ from "lodash"
import { connect } from "react-redux"
import { getNotes, saveNote } from "../actions/notesAction"

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      title: "",
      body: ""
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.props.getNotes()
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    const note = {
      title: this.state.title,
      body: this.state.body
    }
    this.props.saveNote(note)
    this.setState({
      title: "",
      body: ""
    })
  }

  render() {
    let renderNotes = _.map(this.props.notes, (note, key) => {
      return (
        <div key={key}>
          <h2>{note.title}</h2>
          <p>{note.body}</p>
        </div>
      )})
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input
                  onChange={this.handleChange}
                  value={this.state.title}
                  type="text"
                  name="title"
                  className="form-control no-border"
                  placeholder="Title..."
                  required>
                </input>
              </div>

              <div className="form-group">
                <textarea
                  onChange={this.handleChange}
                  value={this.state.body}
                  type="text"
                  name="body"
                  className="form-control no-border"
                  placeholder="Body..."
                  required>
                </textarea>
              </div>

              <div className="form-group">
                <button className="btn btn-primary col-sm-12">Save</button>
              </div>
            </form>
            {renderNotes}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    notes: state.notes
  }
}

const mapDispatchToProps = dispatch => {
  return ({
    getNotes: () => { dispatch(getNotes()) },
    saveNote: (note) => { dispatch(saveNote(note)) }
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
