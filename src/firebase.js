import * as firebase from "firebase"

const config = {
  apiKey: "AIzaSyCIE8vW86N02Uv_4jucBFSG43FIOJ5aUBI",
  authDomain: "diary-1b8d7.firebaseapp.com",
  databaseURL: "https://diary-1b8d7.firebaseio.com",
  projectId: "diary-1b8d7",
  storageBucket: "diary-1b8d7.appspot.com",
  messagingSenderId: "197545074560"
}
firebase.initializeApp(config)

export const database = firebase.database().ref('/notes')
