import { combineReducers } from "redux"
import { notesReducer } from "./notesReducer"

const allReducers = combineReducers({
  notes: notesReducer
})

export default allReducers
