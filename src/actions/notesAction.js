import { database } from "../firebase"

export const GET_NOTES = "GET_NOTES"

export function getNotes() {
  return dispatch => {
    database.on("value", snapshot => {
      dispatch({
        type: GET_NOTES,
        payload: snapshot.val()
      })
    })
  }
}

export function saveNote(note) {
  return dispatch => database.push(note)
}
